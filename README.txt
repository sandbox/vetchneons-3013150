CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module connects to the AWeber API and adds a signup to mailing list block to the site.

A valid AWeber API Development account is required.  Create an account here: https://labs.aweber.com/auth/login 

 * AWeber API Application Permissions:
   Request Subscriber Data

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/aweber_signup


REQUIREMENTS
------------

This module requires the following modules:

 * PHP 7
 * Guzzle
 * OAuth PECL lIBRARY


INSTALLATION
------------

 * To install the OAuth PECL library: pecl install oauth.
   Helpful guide/troublshooting on how to install OAuth PECL library for drupal:
   https://drupal.stackexchange.com/questions/227405/how-to-install-oauth-using-php-7-and-pecl-oauth-library

 * To insall Guzzle with Composer, composer require:
   "guzzlehttp/guzzle": "~6.0",
   "guzzlehttp/oauth-subscriber": "0.3.*"

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules 
   for further information.


CONFIGURATION
-------------

This module requires

	* AWeber Consumer Key and Aweber Consumer Access
	* To authenticate AWeber API with Developer Application:

		/admin/config/services/aweber_signup


MAINTAINERS
-----------

Current maintainers:
 * Steven Luongo (vetchneons) - https://www.drupal.org/u/vetchneons

This project has been sponsored by:
 * Herkimer Media
   With an office in Madison, WI, Herkimer, LLC provides Web Development, 
   creation of marketing collateral materials, and photography services to 
   companies and organizations worldwide. We're a tight-knit team of Web developers, 
   creatives, and business consultants dedicated to the goals of increasing our clients' 
   sales, enhancing their productivity, and delivering Web experiences that are innovative, 
   compelling, and effective.

   Visit: https://herkimer.media/ for more information.
