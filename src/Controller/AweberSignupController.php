<?php

namespace Drupal\aweber_signup\Controller;

require 'vendor/autoload.php';

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;


use Drupal\user\Entity\User;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * Provides a class for Form operations built into Drupal.
 */

class AweberSignupController extends ControllerBase {



 public static function authenticateSendAweber($consumerKey, $consumerSecret) {

 	// Callback url
	$callbackURL = 'http://'.$_SERVER['HTTP_HOST'].'/aweber/authenticate/receive';
	
	// Create a Guzzle client configured to use OAuth for authentication
	$stack = HandlerStack::create();
	$client = new Client([
	    'base_uri' => 'https://auth.aweber.com/1.0/oauth/request_token',
	    'handler' => $stack,
	    'auth' => 'oauth'
	]);

	// Use your consumer key and secret to get a request token and secret.d
	$requestMiddleware = new Oauth1([
	    'consumer_key' => $consumerKey,
	    'consumer_secret' => $consumerSecret,
	    'token' => null,
	    'token_secret' => null,
	]);
	$stack->push($requestMiddleware);
	$res = $client->post('request_token', ['form_params' => ['oauth_callback' => $callbackURL]]);
	$params = [];
	$oauth_info = parse_str($res->getBody(), $params);
	$oauth_info = $params;
	$request_token = $oauth_info['oauth_token'];
	$request_token_secret = $oauth_info['oauth_token_secret'];

	// Save the oauth token secret so we can use it in the callback
  \Drupal::configFactory()->getEditable('aweber_signup.settings')
    ->set('request_token', $request_token)
    ->set('request_token_secret', $request_token_secret)

    ->save();

 }




	public static function authenticateReceiveAweber() {
		// Redirect back to the form and set message authenticated
		$request = \Drupal::requestStack()->getCurrentRequest()->query->all();

		$config = \Drupal::config('aweber_signup.settings');
		$consumerKey = $config->get('consumer_key');
		$consumerSecret = $config->get('consumer_secret');
		$request_token = $config->get('request_token');
		$request_token_secret = $config->get('request_token_secret');

		$verifier = $request['oauth_verifier'];

		// Create a Guzzle client configured to use OAuth for authentication
		$stack = HandlerStack::create();
		$client = new Client([
		    'base_uri' => 'https://auth.aweber.com/1.0/oauth/access_token',
		    'handler' => $stack,
		    'auth' => 'oauth'
		]);

		// Trade the request token and secret and verifier for an access code and secret
		$accessMiddleware = new Oauth1([
		    'consumer_key'    => $consumerKey,
		    'consumer_secret' => $consumerSecret,
		    'token' => $request_token,
		    'token_secret' => $request_token_secret,
		]);

		$stack->push($accessMiddleware);
		$res = $client->post('access_token', ['form_params' => ['oauth_verifier' => $verifier]]);

		$credentials = [];
		parse_str($res->getBody(), $credentials);

		// Save the oauth token secret so we can use it in the callback
		\Drupal::configFactory()->getEditable('aweber_signup.settings')
		  ->set('access_token', $credentials['oauth_token'])
		  ->set('access_token_secret', $credentials['oauth_token_secret'])
		  ->set('access_verifier', $verifier)
		  ->save();

		// Redirect back to the form
		$route = 'aweber_signup.admin'; // your system route
		$url = Url::fromRoute($route)->toString(); // 'cast' to Url 
		$redirect = new RedirectResponse($url); // and redirect


		return $redirect;

	}

	// Make a request to the AWeber API
	public static function makeAweberRequest() {
		$config = \Drupal::config('aweber_signup.settings');
		$consumerKey = $config->get('consumer_key');
		$consumerSecret = $config->get('consumer_secret');
		$access_token = $config->get('access_token');
		$access_token_secret = $config->get('access_token_secret');


		// Create a Guzzle client configured to use OAuth for authentication
		$stack = HandlerStack::create();
		$client = new Client([
		    'base_uri' => 'https://api.aweber.com/1.0/',
		    'handler' => $stack,
		    'auth' => 'oauth'
		]);

		// Trade the request token and secret and verifier for an access code and secret
		$accessMiddleware = new Oauth1([
	    'consumer_key'    => $consumerKey,
	    'consumer_secret' => $consumerSecret,
	    'token' => $access_token,
	    'token_secret' => $access_token_secret,
		]);

		$stack->push($accessMiddleware);

		return $client;
	}	

	// Build a collection of AWeber GET Data based on an AWeber API request
	public static function getCollection($client, $url) {
	    $collection = array();
	    while (isset($url)) {
        $body = $client->get($url)->getBody();
        $page = json_decode($body, true);
        $collection = array_merge($page['entries'], $collection);
        $url = isset($page['next_collection_link']) ? $page['next_collection_link'] : null;
	    }
	    return $collection;
	}
}
