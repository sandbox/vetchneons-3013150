<?php

namespace Drupal\aweber_signup\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\aweber_signup\Controller\AweberSignupController;

/**
 * Configure AWeber API settings for this site.
 */
class AweberSignupBlockForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'aweber_signup_block_form';
  }

	public function buildForm(array $form, FormStateInterface $form_state) {

		$form['aweber_email'] = array(
  		'#type' => 'email',
  		'#title' => t('Email'),
		);

    $form['submit_aweber_email_actions'] = array('#type' => 'actions');
    $form['submit_aweber_email_actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Sign Up'),
      '#attributes'   =>array('class' => array('btn btn-info')),
      '#submit' => ['::submitForm'],
    );

    return $form;

  }

  // //validation hook
  public function validateForm(array &$form, FormStateInterface $form_state) {
  	// Set up messenger service to use later
  	$messenger = \Drupal::messenger();

		// Store the variable
		$aweber_email = $form_state->getValue('aweber_email');

		// Make new api request and store in variable
		$client = AweberSignupController::makeAweberRequest();

		// Get all the list entries for the first account
		$accounts = AweberSignupController::getCollection($client, 'accounts');
		$accountUrl = $accounts[0]['self_link'];

		$listsUrl = $accounts[0]['lists_collection_link'];
		$lists = AweberSignupController::getCollection($client, $listsUrl);    
		
		// Check to see if the email is already subscribed
		$params = array(
	    'ws.op' => 'find',
	    'email' => $aweber_email
		);
		// Query aweber api for the subsriber
		$subsUrl = $lists[0]['subscribers_collection_link'];
		$findUrl = $subsUrl . '?' . http_build_query($params);
		$foundSubscribers = AweberSignupController::getCollection($client, $findUrl);

		// If the email is not already subscribed, add new subsrbier to the list
	  if(empty($foundSubscribers)) {
	    $data = array(
	      'email' => $aweber_email,
	    );
	    $body = $client->post($subsUrl, ['json' => $data]);
	    // Get the subscriber entry using the Location header from the post request
	    $subscriberUrl = $body->getHeader('Location')[0];
	    $subscriberResponse = $client->get($subscriberUrl)->getBody();
	    $subscriber = json_decode($subscriberResponse, true);
	  
	  	// Sets drupal message after form submit
			$messenger->addMessage('Thank you for subscribing!', $messenger::TYPE_STATUS);
		} else {
			// Set form state error saying we are already subscribed
			$form_state->setErrorByName('aweber_email', 'The email: "'.$aweber_email. '" is already in use');
		}

		// Validate form
    parent::validateForm($form, $form_state);
	}
	
  public function submitForm(array &$form, FormStateInterface $form_state) {

		// Get the current url so we can redirect back to our current page on form submit
		$current_route = \Drupal::routeMatch()->getRouteName();
    $url = \Drupal\Core\Url::fromRoute($current_route);
		$form_state->setRedirectUrl($url);

		// Submit form
    parent::submitForm($form, $form_state);
	    	
  }

 }