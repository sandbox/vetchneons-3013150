<?php

namespace Drupal\aweber_signup\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

use Drupal\aweber_signup\Controller\AweberSignupController;


/**
 * Configure AWeber API settings for this site.
 */
class AweberSignupAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'aweber_signup_admin_settings';
  }

  protected function getEditableConfigNames() {
    return ['aweber_signup.settings'];
  }

  /**
   * {@inheritdoc}
   */
   public function buildForm(array $form, FormStateInterface $form_state) {
    
    // Load the config for the module
    $config = $this->config('aweber_signup.settings');
    
    // Provide a link to the AWeber Developer Lab to generate or access the consumer key and consumer secret
    $aw_api_url = Url::fromUri('https://labs.aweber.com', array('attributes' => array('target' => '_blank')));
    
    // Build our form to store our consumer key and consumer secret that is used to authenticate with AWeber API
    $form['consumer_key'] = array(
      '#type' => 'textfield',
      '#title' => t('AWeber Consumer Key'),
      '#required' => TRUE,
      '#default_value' => $config->get('consumer_key'),
      '#description' => t('The consumer Key for your AWeber API Project. Get or generate a valid API key at your @apilink.', array('@apilink' => Link::fromTextAndUrl(t('AWeber API Dashboard'), $aw_api_url)->toString())),
    );
    $form['consumer_secret'] = array(
      '#type' => 'textfield',
      '#title' => t('AWeber Consumer Secret'),
      '#required' => TRUE,
      '#default_value' => $config->get('consumer_secret'),
      '#description' => t('The consumer secret for your AWeber API Project. Get or generate a valid API key at your @apilink.', array('@apilink' => Link::fromTextAndUrl(t('AWeber API Dashboard'), $aw_api_url)->toString())),
    );

    // Show the AWeber Account Authroization Link if we have a request token stored in our site conifgs
    if($request_token = $config->get('request_token')) {
      $form['auth'] = array(
        '#type' => 'fieldset',
        '#title' => t('Authenticate AWeber'),
        '#weight' => 100,
      );
      $form['auth']['auth_link'] = array(
        '#markup' => 'To either authenticate or reauthenticate: <a href ="https://auth.aweber.com/1.0/oauth/authorize?oauth_token='.$request_token.'"> Click here to login</a> after saving this configuration with the key and secret',
      );
    }

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the consumer key and the consumer secret on form submit
    $consumerKey = $form_state->getValue('consumer_key');
    $consumerSecret = $form_state->getValue('consumer_secret');

    $config = $this->config('aweber_signup.settings');
    $config
      ->set('consumer_key', $form_state->getValue('consumer_key'))
      ->set('consumer_secret', $form_state->getValue('consumer_secret'))
      ->save();

    // Pass our consumer key and consumer secret to the controller 
    AweberSignupController::authenticateSendAweber($consumerKey, $consumerSecret);

    // Show instruction method on page if we authenticated properly
    \Drupal::messenger()->addMessage(t('Please authenticate AWeber by logging into your Aweber Account in the "AUTHENTICATE AWEBER" section at the bottom of the form'), 'warning');

    parent::submitForm($form, $form_state);
  }
}



