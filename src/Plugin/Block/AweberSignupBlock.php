<?php

namespace Drupal\aweber_signup\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;
use Drupal\aweber_signup\Form\AweberSignupBlockForm;


/**
 * Provides a Aweber List Signup Block.
 *
 * @Block(
 *   id = "aweber_signup_block",
 *   admin_label = @Translation("Aweber Signup Block"),
 *   category = @Translation("Aweber Signup Block"),
 * )
 */
class AweberSignupBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
   	$form = \Drupal::formBuilder()->getForm('Drupal\aweber_signup\Form\AweberSignupBlockForm');
    return $form;
  }

}